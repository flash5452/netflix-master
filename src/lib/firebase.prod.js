import Firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

// 1) when seeding the database you'll have to uncomment this!
//import { seedDatabase } from '../seed';

const config = {
  apiKey: "AIzaSyChOP2--aUExMx7NMCifm25PTA9S4L5TFo",
  authDomain: "netflix-master-39276.firebaseapp.com",
  databaseURL: "https://netflix-master-39276-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "netflix-master-39276",
  storageBucket: "netflix-master-39276.appspot.com",
  messagingSenderId: "151111608615",
  appId: "1:151111608615:web:44162136770ebae1c02102"
};

const firebase = Firebase.initializeApp(config);
// 2) when seeding the database you'll have to uncomment this!
//seedDatabase(firebase);
// 3) once you have populated the database (only run once!), re-comment this so you don't get duplicate data

export { firebase };
